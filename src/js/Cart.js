import React from 'react';
//import '../css/ImageList.css';
import { connect } from 'react-redux';

import { removeFromCart } from '../actions';


const Cart = props => {
    
    const images = props.images.map(({ id, image, name, price }) => {
        return (
            <div key={id}>
                <img
                    src={image}
                    alt={name}
                />
                Name: {name}
                <br />
                Price: ${price}
                <br />
                <button onClick={() => props.removeFromCart(
                    { 
                        id: id,
                        image: image,
                        name: name,
                        price: price
                    })}>
                    Remove from cart
                </button>
            </div>
                );
    });

    return <div className="image-list"><h2>Cart:</h2><br />{images}<br /><button>Checkout</button></div>;
}

const mapStateToProps = (state) => {
    return { 
        images: state.cartReducer
    };
}

export default connect(mapStateToProps, {
    removeFromCart: removeFromCart
})(Cart);