import React from 'react';
import '../css/StartMenu.css';
import LoremIpsumLogo from '../img/LoremIpsumLogo.jpg';

const StartMenu = () => {

    return (
        <div>
            <img    id="logo"    
                    src={LoremIpsumLogo}
                    alt="logo"
            />
            <div id="account-area">
                <button>
                    <a href="/login">
                        Login
                    </a>
                </button>
                
                <button>
                    <a href="/signup">
                        Cadastro
                    </a>
                </button>
                
            </div>
        </div>
    );
};

export default StartMenu;

/* References:

https://www.w3schools.com/react/react_css.asp
https://stackoverflow.com/questions/39999367/how-do-i-reference-a-local-image-in-react

*/