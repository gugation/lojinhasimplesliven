import React from 'react';
import { connect } from 'react-redux';

import '../css/ImageList.css';
import { addToCart } from '../actions';

const ImageList = props => {
    
    const images = props.images.map(({ id, image, name, price }) => {
        return (
            <div key={id}>
                <img
                    src={image}
                    alt={name}
                />
                Name: {name}
                <br />
                Price: ${price}
                <br />
                <button onClick={() => props.addToCart(
                    { 
                        id: id,
                        image: image,
                        name: name,
                        price: price
                    })}>
                    Add to cart
                </button>
            </div>
                );
    });

    return <div className="image-list"><h2>Products:</h2><br />{images}</div>;
}

const mapStateToProps = (state) => {
    return {};
}

export default connect(mapStateToProps, {
    addToCart: addToCart
})(ImageList);