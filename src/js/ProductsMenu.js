import React from 'react';

import '../css/ProductsMenu.css';
import axios from 'axios';
import ImageList from './ImageList.js';
import Cart from './Cart';


/*
    data: Array(50)
        0:
            createdAt: "2019-09-02T12:58:54.103Z"
            id: "1"
            image: "http://lorempixel.com/640/480/food"
            name: "Rustic Metal Fish"
            price: "289.00"
            stock: 65171
*/

class ProductsMenu extends React.Component {
    state = { images: [] };
    
    onSearchSubmit = async () => {
        const response = await axios.get('https://5d6da1df777f670014036125.mockapi.io/api/v1/product');

        this.setState({ images: response.data });
    };
    
    render(){
        if (this.state.images.length === 0) this.onSearchSubmit();
        return (
            <div className='products-cart'>
                <div className="products">
                    <ImageList images={this.state.images.slice(0, 10)} />
                </div>
                <div className="cart">
                    <Cart />
                </div>
            </div>
        );
    };
};

export default ProductsMenu;