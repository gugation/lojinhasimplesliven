import React from 'react';

import '../css/SignUpMenu.css';

class SignUpMenu extends React.Component {
    state = {
        'name': '',
        'email': '',
        'password': '',
        'birth1': '',
        'birth2': '',
        'birth3': '',
        'cpf1': '',
        'cpf2': '',
        'cpf3': '',
        'cpf4': '',
        'status' : ''
    };


    validateInput = () => {
        //Other validations

        if(this.props.registerUser(this.state) === false){
            this.setState({'status': 'Email/Cpf already exists'})
            return false;
        }

        this.setState({'status': 'User signed up!'});
        return true;
    }
    
    render(){
        return (
        <main>
            {this.state.status}
            <div className="signup-company">
                <div className="company-name">
                    Lorem Ipsum Slogan
                </div>
                <div className="login-user">
                    <div className="user-input">
                        <br />
                        Name:   <input 
                                    value={this.state.name}
                                    onChange={(e) => this.setState({ name: e.target.value })} 
                                />
                        <br />
                        <br />
                        Email: <input 
                                    value={this.state.email}
                                    onChange={(e) => this.setState({ email: e.target.value })} 
                                />
                        <br />
                        <br />
                        Password: <input 
                                    value={this.state.password}
                                    onChange={(e) => this.setState({ password: e.target.value })} 
                                />
                        <br />
                        <br />
                        Date of birth: 
                        <input   style={{width: '19px'}} 
                                value={this.state.birth1}
                                onChange={(e) => this.setState({ birth1: e.target.value })} 
                        /> 
                        / 
                        <input   style={{width: '19px'}} 
                                value={this.state.birth2}
                                onChange={(e) => this.setState({ birth2: e.target.value })} 
                        /> 
                        / 
                        <input   style={{width: '34px'}} 
                                value={this.state.birth3}
                                onChange={(e) => this.setState({ birth3: e.target.value })} 
                        /> 
                        <br />
                        <br />
                        CPF: 
                        <input  style={{width: '28px'}}
                                value={this.state.cpf1}
                                onChange={(e) => this.setState({ cpf1: e.target.value })}
                        />
                         - 
                        <input  style={{width: '28px'}}
                                value={this.state.cpf2}
                                onChange={(e) => this.setState({ cpf2: e.target.value })}
                        />
                         - 
                        <input  style={{width: '28px'}}
                                value={this.state.cpf3}
                                onChange={(e) => this.setState({ cpf3: e.target.value })}
                        />
                         . 
                        <input  style={{width: '19px'}}
                                value={this.state.cpf4}
                                onChange={(e) => this.setState({ cpf4: e.target.value })}
                        />
                    </div>
                    <div className="signup-btn">
                        <br />
                        <button onClick={() => this.validateInput()}>
                            <a  {
                                    ...this.state.status === 'User signed up!'
                                    ? {href: '/products'}
                                    : ''
                                } >
                                Sign up
                            </a>
                        </button>
                    </div>
                </div>
            </div>
        </main>
        );
    }
};

export default SignUpMenu;

/* References:

https://thoughtbot.com/blog/positioning#flexbox

*/