import React from 'react';
import StartMenu from './StartMenu';
import LoginMenu from './LoginMenu';
import SignUpMenu from './SignUpMenu';
import ProductsMenu from './ProductsMenu';
import Route from './Route';
import Header from './Header';

const userData = [
    {
        'name': 'Ademiro',
        'email': 'admin',
        'password': 'admin',
        'birth1': '01',
        'birth2': '01',
        'birth3': '2000',
        'cpf1': '123',
        'cpf2': '123',
        'cpf3': '123',
        'cpf4': '12'
    },
    {
        'name': 'Bernardinho',
        'email': 'bnd@gmail.com',
        'password': 'littlebernard',
        'birth1': '11',
        'birth2': '11',
        'birth3': '2001',
        'cpf1': '213',
        'cpf2': '213',
        'cpf3': '213',
        'cpf4': '21'
    }];

class App extends React.Component{
    state = {
                isLoggedIn: 'false',
                activeUserIndex: null
            };
    
    setActiveUser = (index) => {
        this.setState({activeUserIndex: index});
    }

    registerUser = (newUserData) => {
        for(let i = 0; i < userData.length; i++){
            const cpf = userData[i]['cpf1'] + userData[i]['cpf2']
            + userData[i]['cpf3'] + userData[i]['cpf4'];
            
            const newCpf = newUserData['cpf1'] + newUserData['cpf2']
            + newUserData['cpf3'] + newUserData['cpf4'];
            
            if(userData[i]['email'] === newUserData['email'] || cpf === newCpf)
                return false;
        }
        
        userData.push(newUserData);
        this.setActiveUser(userData.length - 1);
        console.log(this.activeUserIndex);
        return true;
    }

    getUserName = () => {
        return this.state.activeUserIndex !== null 
        ? userData[this.state.activeUserIndex].name 
        : 'Visitante';
    }

    render() {
        return(
            <div>
                <Header userName={this.getUserName()}/>
                <div style={{'marginTop': '100px'}}>
                    <Route path="/">
                        <StartMenu />
                    </Route>
                    <Route path="/login">
                        <LoginMenu />
                    </Route>
                    <Route path="/signup">
                        <SignUpMenu registerUser={this.registerUser} />
                    </Route>
                    <Route path="/products">
                        <ProductsMenu />
                    </Route>
                </div>
            </div>
        );
    }
};

export default App;

/* References:

    https://www.w3schools.com/react/react_css.asp
    https://stackoverflow.com/questions/39999367/how-do-i-reference-a-local-image-in-react

*/