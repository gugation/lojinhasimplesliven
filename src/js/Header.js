import React from 'react';
import { connect } from 'react-redux';

import LoremIpsumLogo from '../img/LoremIpsumLogo.jpg';
import '../css/Header.css';
import { selectActiveUser } from '../actions';


const Header = (props) => {
    return (
        <header id="header">
            <div className="logo">
                <a href="/">
                    <img id="header-img"
                        src={LoremIpsumLogo}
                        alt="lorem ipsum logo"
                    />
                </a>
            </div>
            <nav id="nav-bar">
                Bem vindo, {props.selectedUserIndex !== null ? props.users[props.selectedUserIndex].name : "Visitante"}
            </nav>  
        </header>
    );
};

const mapStateToProps = (state) => {
    
    return {    users: state.users,
                selectedUserIndex: state.selectedUserReducer
            };
}

export default connect(mapStateToProps, {
    selectActiveUser: selectActiveUser
})(Header);