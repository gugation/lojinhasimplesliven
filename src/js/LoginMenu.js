import React from 'react';
import { connect } from 'react-redux';
import { selectActiveUser } from '../actions';

import '../css/LoginMenu.css';

class LoginMenu extends React.Component {
    state = {
        email: '',
        password: '',
        status: ''
    }
    
    checkCredentials = () => {
        for(let i = 0; i < this.props.users.length; i++){
            
            if(this.state.email === this.props.users[i].email){
                
                if(this.state.password === this.props.users[i].password){
                    this.setState({status: 'Logged in successfully!'});
                    this.props.selectActiveUser(i);
                    return true;
                }

                else{
                    this.setState({status: 'Incorrect password.'});
                    return false;
                }

            }  

        }

        this.setState({status: 'User not found.'});
        return false;
    }

    render(){
        return (
        <div>
            {this.state.status}
            <div className="login-company">
                <div className="company-name">
                    Lorem Ipsum Slogan
                </div>
                <div className="login-user">
                    <div className="user-input">
                        <br />
                        Email: 
                        <input 
                            value={this.state.email}
                            onChange={(e) => this.setState({ email: e.target.value })} 
                        />
                        <br />
                        <br />
                        Password: 
                        <input 
                            value={this.state.password}
                            onChange={(e) => this.setState({ password: e.target.value })} 
                        />
                    </div>
                    <div className="login-btn">
                        <br />
                        <button onClick={() => this.checkCredentials()}>
                            <a  {
                                ...this.state.status === 'Logged in successfully!'
                                ? {href: '/products'}
                                : ''
                            } >
                                Login
                            </a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        );
    }
};

const mapStateToProps = (state) => {
    console.log(state);
    return { users: state.users };
}

export default connect(mapStateToProps, {
    selectActiveUser: selectActiveUser
})(LoginMenu);

/* References:

https://thoughtbot.com/blog/positioning#flexbox
https://stackoverflow.com/questions/51997751/jsx-inline-conditional-attribute-href

*/