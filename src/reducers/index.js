import { combineReducers } from 'redux';

const usersReducer = () => {
    return [
        {
            'name': 'Ademiro',
            'email': 'admin',
            'password': 'admin',
            'birth1': '01',
            'birth2': '01',
            'birth3': '2000',
            'cpf1': '123',
            'cpf2': '123',
            'cpf3': '123',
            'cpf4': '12'
        },
        {
            'name': 'Bernardinho',
            'email': 'bnd@gmail.com',
            'password': 'littlebernard',
            'birth1': '11',
            'birth2': '11',
            'birth3': '2001',
            'cpf1': '213',
            'cpf2': '213',
            'cpf3': '213',
            'cpf4': '21'
        }
    ];
};

const selectedUserReducer = (user = null, action) => {
    if(action.type === 'ACTIVE_USER_SELECTED'){
        return action.payload;
    }

    return user;
};

const cartReducer = (items = [], action) => {
    if(action.type === 'ADD_CART'){
        return [...items, action.payload];
    }

    if(action.type === 'REMOVE_CART'){
        return items.filter(item => item.id !== action.payload.id);
    }

    return items;
};

export default combineReducers({
    users: usersReducer,
    selectedUserReducer: selectedUserReducer,
    cartReducer: cartReducer
});